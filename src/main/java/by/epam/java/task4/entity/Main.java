package by.epam.java.task4.entity;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {

	List<Passenger> passengers = new ArrayList<>();

	for (int i = 1; i < 5; i++) {
	    if ((int) (Math.random() * 2) > 0)
		passengers.add(new Passenger(i, i + 1));
	    if ((int) (Math.random() * 2) > 0)
		passengers.add(new Passenger(i, 5));
	}

	Bus.passengers = passengers;

	new Thread(new Bus(0, 6, 100)).start();
	Thread.sleep(1);

	//new Thread(new Bus(0, 1, 91)).start();

    }

}
