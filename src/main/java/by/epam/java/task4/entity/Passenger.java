package by.epam.java.task4.entity;

public class Passenger extends Thread {

	private int departure;
	private int destination;

	public Passenger(int departure, int destination) {
		this.departure = departure;
		this.destination = destination;
		System.out.println(this + " is waiting at the bus stop # " + this.getDeparture());
	}

	@Override
	public void run() {
		try {
			System.out.println(this + " boarded the bus.");

			while (Bus.getPhaser().getPhase() < destination)
				Bus.getPhaser().arriveAndAwaitAdvance();
			Thread.sleep(1);

			System.out.println(this + " left the bus.");

			Bus.getPhaser().arriveAndDeregister();
		} catch (InterruptedException e) {
		}
	}

	public int getDestination() {
		return destination;
	}

	public void setDestination(int destination) {
		this.destination = destination;
	}

	public int getDeparture() {
		return departure;
	}

	public void setDeparture(int departure) {
		this.departure = departure;
	}

	@Override
	public String toString() {
		return "Passenger from " + getDeparture() + " to " + getDestination();
	}
}
