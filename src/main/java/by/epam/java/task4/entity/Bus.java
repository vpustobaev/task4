package by.epam.java.task4.entity;

import java.util.List;
import java.util.concurrent.Phaser;

public class Bus extends Thread {

    public static List<Passenger> passengers;
    private int startingDepot;
    private int endingDepot;
    private int busNumber;

    public Bus(int startingDepot, int endingDepot, int busNumber) {
	this.setStartingDepot(startingDepot);
	this.setEndingDepot(endingDepot);
	this.busNumber = busNumber;
    }

    private static final Phaser PHASER = new Phaser(1);

    public static Phaser getPhaser() {
	return PHASER;
    }

    @Override
    public void run() {

	for (int i = 0; i < 7; i++) {
	    switch (i) {
	    case 0:
		System.out.println("The bus # " + this.busNumber + " from " + this.getStartingDepot() + " to "
			+ this.getEndingDepot() + " left the depo.");
		PHASER.arrive();
		break;
	    case 6:
		System.out.println("The bus # " + this.busNumber + " left to the depo.");
		PHASER.arriveAndDeregister();
		break;
	    default:
		int currentBusStop = PHASER.getPhase();

		System.out.println("The bus # " + this.busNumber + " arived at Busstop # " + currentBusStop);

		for (Passenger p : passengers)
		    if (p.getDestination() < this.getEndingDepot()) {
			if (p.getDeparture() == currentBusStop) {
			    PHASER.register();
			    p.start();
			}
		    } else {
			PHASER.arriveAndAwaitAdvance();
		    }

		PHASER.arriveAndAwaitAdvance();

	    }
	}
    }

    public int getBusNumber() {
	return busNumber;
    }

    public void setBusNumber(int busNumber) {
	this.busNumber = busNumber;
    }

    public void setPassengers(List<Passenger> passengers) {
	this.passengers = passengers;
    }

    public List<Passenger> getPassengers() {
	return passengers;
    }

    public int getStartingDepot() {
	return startingDepot;
    }

    public void setStartingDepot(int startingDepot) {
	this.startingDepot = startingDepot;
    }

    public int getEndingDepot() {
	return endingDepot;
    }

    public void setEndingDepot(int endingDepot) {
	this.endingDepot = endingDepot;
    }

}
